def selection_sort(unsorted):

    unsorted_list = list(unsorted)
    """Making shallow copy of the unsorted list"""
    unsorted_copy = list(unsorted)

    if len(unsorted) == 0:
       raise("Error!list is empty")
    final_list = []
    while len(unsorted_copy) > 0:
        smallest_number = unsorted_copy[0]
        for i in range(0,len(unsorted_copy)):
            if unsorted_copy[i] < smallest_number:
                smallest_number = unsorted_copy[i]
        final_list.append(smallest_number)
        unsorted_copy.remove(smallest_number)
    return(final_list)
    # print(final_list)
    # print(unsorted_copy)
selection_sort([1,2,3,-1])