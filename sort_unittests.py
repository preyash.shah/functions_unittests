import unittest
from sort import selection_sort


class selection_sort_tests(unittest.TestCase):
    def test_equal_values_case_1(self):
        lst = [2,2]
        sorted_list = selection_sort(lst)
        print()
        self.assertEqual(sorted_list,lst)

    def test_opposite_values_case_2(self):
        lst = [-2, 2]
        sorted_list = selection_sort(lst)
        self.assertEqual([-2,2], sorted_list)

    def test_extreme_values_case_3(self):
        lst = [-10000000, 1000000, 0]
        sorted_list = selection_sort(lst)
        self.assertEqual(sorted_list,[-10000000,0, 1000000])
        
"""To make them importable """
if __name__ == '__main__':
    unittest.main()